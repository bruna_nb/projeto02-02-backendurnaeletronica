const express = require("express")
const fs = require("fs")

const path = require("path")

const app = express()

app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(express.static("public"))
app.use(express.static(path.join(__dirname, "public")));

const cors = require("cors");
app.use(cors());
const porta = 3001;

app.listen(porta, function(){ })

app.get("/", function(req, resp){
    resp.sendFile(path.join(__dirname + "../../exercicio02_1-frontend-urna-eletronica/index.html"))
})

app.get("/cargainicial", function (req, resp) {
    fs.readFile("config.csv", "utf-8", function (err, data) {
        const dados = data.split("\r\n")
        dados.splice(dados.length - 1, 1)
        const candidatos = []
        for (candidato of dados) {
            let dadosCandidato = candidato.split(";")
            candidatos.push(dadosCandidato)
        }
        resp.send(candidatos)
    })
})

app.post("/voto", function (req, resp) {
    let voto = req.body.rg + ";" + req.body.voto + ";" + req.body.timestamp + "\n"

    fs.appendFile("votos.csv", voto, function (err) {
        if (err) {
            resp.json({
                "Status": "500",
                "Mensagem": "Erro ao registrar voto, contate o administrador do sistema"
            })
            throw err
        } else {
            resp.json({
                "Status": "200",
                "Mensagem": "Voto Registrado Com sucesso"
            })
        }

    })
})

app.get("/apuracao", async function (req, resp) {
    const data = fs.readFileSync("config.csv", "utf-8", "r")
    let candidatos = []
    const dados = data.split("\r\n")
    dados.splice(dados.length - 1, 1)
    for (candidato of dados) {
        let dadosCandidato = candidato.split(";")
        dadosCandidato.push(0)
        dadosCandidato.splice(0,1)
        candidatos.push(dadosCandidato)
    }
    fs.readFile("votos.csv", "utf-8", function (err, data) {
        const dadosVotos = data.split("\n")
        dadosVotos.splice(dadosVotos.length - 1, 1)
        let votos = []
        for (voto of dadosVotos){
            let dadosVoto = voto.split(";")
            votos.push(dadosVoto)
        }
        let vtsEmBranco = 0
        let vtsNulos = 0
        for (voto of votos) {
            let votou = false
            for (candidato of candidatos) {
                if (voto[1] == candidato[0]) {
                    candidato[3] += 1
                    votou = true
                    break
                }
            }
            if (!votou) {
                if (voto[1] == "") {
                    vtsEmBranco += 1
                }
                else {
                    vtsNulos += 1
                }
            }
        }
        candidatos.sort(function(a,b){
            if(a[3] < b[3]){
                return 1
            }else if(a[3] > b[3]){
                return -1
            }else{
                return 0
            }
        })

        let resultado = {
            apuracao: candidatos,
            vtsEmBranco: vtsEmBranco,
            vtsNulos: vtsNulos
        }
        resp.send(resultado)
    })

})

app.get("/apuracaoidentificada", async function (req, resp) {
    const data = fs.readFileSync("config.csv", "utf-8", "r")
    let candidatos = []
    const dados = data.split("\r\n")
    dados.splice(dados.length - 1, 1)
    for (candidato of dados) {
        let dadosCandidato = candidato.split(";")
        dadosCandidato.push(0)
        dadosCandidato.splice(0,1)
        candidatos.push(dadosCandidato)
    }
    fs.readFile("votos.csv", "utf-8", function (err, data) {
        const dadosVotos = data.split("\n")
        dadosVotos.splice(dadosVotos.length - 1, 1)
        let votos = []
        let rgs = []
        for (voto of dadosVotos){
            let dadosVoto = voto.split(";")
            votos.push(dadosVoto)
        }
        let vtsEmBranco = 0
        let vtsNulos = 0
        for (voto of votos) {
            let votou = false
            let rgNaoIdentificado = true
            for (rg of rgs){
                if(voto[0] == rg){
                    rgNaoIdentificado = false
                }
            }
            if(rgNaoIdentificado && voto[0] != ""){
                rgs.push(voto[0])
                for (candidato of candidatos) {
                    if (voto[1] == candidato[0]) {
                        candidato[3] += 1
                        votou = true
                        break
                    }
                }
                if (!votou) {
                    if (voto[1] == "") {
                        vtsEmBranco += 1
                    }
                    else {
                        vtsNulos += 1
                    }
                }
            }
        }
        candidatos.sort(function(a,b){
            if(a[3] < b[3]){
                return 1
            }else if(a[3] > b[3]){
                return -1
            }else{
                return 0
            }
        })

        let resultado = {
            apuracao: candidatos,
            vtsEmBranco: vtsEmBranco,
            vtsNulos: vtsNulos
        }
        resp.send(resultado)
    })

})

