const btnApurar = document.getElementById("btnApuracao")
const divPai = document.getElementById("divPrincipal")
const divNulosEBrancos = document.getElementById("divNulosEBrancos")
const divForm = document.getElementById("idDivForm")

btnApurar.addEventListener("click", function(){
    divForm.style.display = "none"
    let radios = document.getElementsByName("nmTipoVotacao")
    let selecionado = ""
    for(let i=0; i < radios.length; i++){
        if(radios[i].checked){
            selecionado = radios[i].id
        }
    }
    if (selecionado == "idVotAnonima" || selecionado == ""){
        apurarVotacaoAnonima()
    }else{
        apurarVotacaoIdentificada()
    }

})

async function apurarVotacaoAnonima(){
    let response = await fetch("http://localhost:3001/apuracao")
    let dados = await response.json()
    mostrarResultado(dados)
}

async function apurarVotacaoIdentificada(){
    let response = await fetch("http://localhost:3001/apuracaoidentificada")
    let dados = await response.json()
    mostrarResultado(dados)
}

function mostrarResultado(dados){
    let linha = criarNovaLinha()
    for (let i = 0; i < dados.apuracao.length; i++) {
        if((i%4) == 0 && i!=1){
            linha = criarNovaLinha()
        }
        criarNovoQuadroCandidato(linha, dados.apuracao[i])
    }
    mostrarNulosEBrancos(dados.vtsEmBranco, dados.vtsNulos)

}

function criarNovaLinha(){
    let novaLinha = document.createElement("div")
    divPai.appendChild(novaLinha)
    novaLinha.classList.add("row", "my-2")
    return novaLinha
}

function criarNovoQuadroCandidato(linha, candidato){
    let cartao = document.createElement("div")
    cartao.classList.add("card", "mx-2", "my-2")
    cartao.style.width = "18rem"

    let imgCandidato = document.createElement("img")
    imgCandidato.classList.add("rd-img-top")
    imgCandidato.alt = "Card image cap"
    cartao.appendChild(imgCandidato)
    imgCandidato.src = candidato[2]

    let cartaoBody = document.createElement("div")
    cartaoBody.classList.add("card-body")
    cartao.appendChild(cartaoBody)

    let cartaoTitulo = document.createElement("h4")
    cartaoTitulo.classList.add("card-title")
    cartaoTitulo.textContent = candidato[1] + ", " + candidato[0]
    cartaoBody.appendChild(cartaoTitulo)

    let nVotos = document.createElement("h6")
    nVotos.classList.add("card-text")
    nVotos.textContent = candidato[3] + " votos"
    cartaoBody.appendChild(nVotos)

    linha.appendChild(cartao)
}

function mostrarNulosEBrancos(votosBrancos, votosNulos){
    criarQuadroNulosOuBrancos(votosBrancos, "Votos em branco")
    criarQuadroNulosOuBrancos(votosNulos, "Votos Nulos")
}

function criarQuadroNulosOuBrancos(nVotos, titulo){
    let cartao = document.createElement("div")
    cartao.classList.add("card", "col", "mx-2")
    cartao.style.width = "18rem"

    let cartaoBody = document.createElement("div")
    cartaoBody.classList.add("card-body")
    cartao.appendChild(cartaoBody)

    let cartaoTitulo = document.createElement("h5")
    cartaoTitulo.classList.add("card-title")
    cartaoTitulo.textContent = titulo
    cartaoBody.appendChild(cartaoTitulo)

    let cartaoSubTitulo = document.createElement("h6")
    cartaoSubTitulo.classList.add("card-subtitle", "mb-2", "text-muted")
    cartaoSubTitulo.textContent = nVotos
    cartaoBody.appendChild(cartaoSubTitulo)

    divNulosEBrancos.appendChild(cartao)
}
