//Declaração de variáveis globais

//Variáveis que serão preenchidas com dados que vem do back-end
var listaDeCandidatos
var candidatos

//Constantes que estão na tela
const saidacand = document.getElementById("idOutCandidato")
const divRG = document.getElementById("idRGEleitor")
const campoRG = document.getElementById("idNumeroRG")
const efeitosonoro = document.querySelector("#idEfeitoSonoro")
const fotocandidato = document.getElementById("FotoCandidato")
const campoVoto = document.getElementById("idNumeroDoCandidato")
const modal = document.getElementById("avisoModal")

//Botões
const btnConfirma = document.getElementById("idBtConfirma")
const btnBranco = document.getElementById("idBtEmBranco")
const btnCloseModal = document.getElementById("idBtnClose")
const btnCorrige = document.getElementById("idBtCancela")
const btnOK = document.getElementById("btnOK")
const btn0 = document.getElementById("idBtn0")
const btn1 = document.getElementById("idBtn1")
const btn2 = document.getElementById("idBtn2")
const btn3 = document.getElementById("idBtn3")
const btn4 = document.getElementById("idBtn4")
const btn5 = document.getElementById("idBtn5")
const btn6 = document.getElementById("idBtn6")
const btn7 = document.getElementById("idBtn7")
const btn8 = document.getElementById("idBtn8")
const btn9 = document.getElementById("idBtn9")

//Outras variáveis globais
var sequenciadigitada=""

//EventListeners dos botões da tela
btnCloseModal.addEventListener("click", function(){
    habilitarBotoes()
})
btnOK.addEventListener("click", function(){
    habilitarBotoes()
})

btn0.addEventListener("click", function(){
    apertar(0)    
})
btn1.addEventListener("click", function(){
    apertar(1)    
})
btn2.addEventListener("click", function(){
    apertar(2)    
})
btn3.addEventListener("click", function(){
    apertar(3)    
})
btn4.addEventListener("click", function(){
    apertar(4)    
})
btn5.addEventListener("click", function(){
    apertar(5)    
})
btn6.addEventListener("click", function(){
    apertar(6)    
})
btn7.addEventListener("click", function(){
    apertar(7)    
})
btn8.addEventListener("click", function(){
    apertar(8)    
})
btn9.addEventListener("click", function(){
    apertar(9)    
})

btnConfirma.addEventListener("click", function(){
    contabilizarVotos()    
})
btnBranco.addEventListener("click", function(){
    campoVoto.value = ""
    contabilizarVotos()
})
btnCorrige.addEventListener("click", function(){
    limparTela()    
})


//Função que irá mostrar o candidato selecionado, seja pelos botões ou pela lista lateral
//Essa função só será chamada quando uma tecla da urna for apertada ou quando um candidato for selecionado na lista
function mostrarCandidato(numCandPreSelecionado){
    //Limpando formatação da lista de candidatos
    for (let i=0; i< listaDeCandidatos.length; i++){
        listaDeCandidatos[i].className = "list-group-item"  
    }
    //Limpando nome do candidato selecionado (que aparece embaixo da foto)
    let selecionado = false
    let caminhoImagem = ""
    //Verficando se o número selecionado é válido e, se sim, atualizando a formatação da lista e 
    //Os parâmetros como imagem e nome do candidato que aparece embaixo dela
    for(candidato of candidatos){
        if (numCandPreSelecionado == candidato[1]){
            selecionado = true
            caminhoImagem = candidato[3]
        }
    }
    fotocandidato.src = caminhoImagem
    if(selecionado){
        let candPreSelecionado = document.getElementById(numCandPreSelecionado)
        candPreSelecionado.classList.add("bg-success")
        selecionado = candPreSelecionado.textContent
        saidacand.innerHTML = "<p class='fs-4'> Candidato Selecionado:</p> <p class = 'fs-3'>"+ selecionado +"</p>"
    }else{
        //Mensagem caso o número selecionado não corresponda a um candidato
        saidacand.innerHTML = "<p class='fs-4 text-danger'> Nenhum candidato válido selecionado.</p><p class='text-danger'>Se você confirmar agora, seu voto será considerado NULO.</p>"
    }
}

function reproduzirSomBotao(caminhoSom){
    efeitosonoro.src = caminhoSom
    efeitosonoro.play()
}

//Função chamada quando um candidato é selecionado na lista lateral
function selecionar(candidatoselecionado){
    campoVoto.value = candidatoselecionado
    sequenciadigitada = ""
    reproduzirSomBotao("http://localhost:3001/sons/botaoapertado.mp3")
    mostrarCandidato(candidatoselecionado)
}
//Função que limpa a tela, chamada quando o botão corrige é clicado ou quando é computado um voto
//Seja ele válido, nulo ou em branco
function limparTela(){
    campoVoto.value = ""
    sequenciadigitada=""
    saidacand.innerHTML=""
    btnCloseModal.style.display = "flex"
    btnOK.style.display = "flex"
    for (let i=0; i< listaDeCandidatos.length; i++){
        listaDeCandidatos[i].className = "list-group-item"  
    }
    fotocandidato.src=""
}

//Função usada para acrescentar votos válidos ou nulos
async function contabilizarVotos(){
    const voto = campoVoto.value
    const rg = campoRG.value
    const data = new Date()
    const timestamp = String(data.getFullYear()) + String(data.getMonth()) + String(data.getDay()) + String(data.getHours()) + String(data.getMinutes()) + String(data.getSeconds()) + String(data.getMilliseconds())
    const update = {
        rg: rg,
        voto: voto,
        timestamp: timestamp
    }

    const options = {
        method: "POST",
        headers: {
            "Content-type": "application/json"
        },
        body: JSON.stringify(update)
    }

    let response = await fetch("http://localhost:3001/voto", options)
    let msg = await response.json()
    exibirModal(msg)
    reproduzirSomBotao("http://localhost:3001/sons/botaoconfirma.mp3")
    limparTela()
}

function exibirModal(msg){
    let modalBody = document.getElementById("idModalBody")
    let modalTitulo = document.getElementById("avisoModalLabel")
    
    limparTela()
    if(msg.Status == "500"){
        btnConfirma.disabled = true
        btnBranco.disabled = true
        modalBody.classList.add("text-danger", "fw-bold")
        modalBody.innerText = msg.Mensagem
        modalTitulo.innerText = "Erro!"

    }else{
        modalBody.classList.add("text-success", "fw-bold")
        modalBody.innerText = msg.Mensagem
        setTimeout(function(){
            modal.classList.remove("show")
            modal.setAttribute("aria-hidden", true)
            modal.style.display = "none"
            let backdrop = document.getElementsByClassName("modal-backdrop")
            document.querySelector("body.modal-open").removeChild(backdrop[0])
        }, 2000)
    }
}


function habilitarBotoes(){
    btnConfirma.disabled = false
    btnBranco.disabled = false
}
//Função chamada quando uma tecla da urna é apertada
function apertar(botaoapertado){
    sequenciadigitada += botaoapertado
    campoVoto.value = sequenciadigitada
    reproduzirSomBotao("http://localhost:3001/sons/botaoapertado.mp3")
    mostrarCandidato(sequenciadigitada)
}

window.onload = async function(){
    let dados = await fetch(`http://localhost:3001/cargainicial`)
    candidatos = await dados.json()
    if(candidatos[0][0] == "A"){
         divRG.style.display = "none"
         campoRG.required = false
    }
    mostrarListaCandidatos(candidatos)
    listaDeCandidatos = document.getElementsByClassName("list-group-item")

}

function mostrarListaCandidatos(candidatos){
    for(candidato of candidatos){
        const novoItemLista = document.createElement("li")
        const lista = document.getElementById("idListaCandidatos")
        novoItemLista.classList.add("list-group-item")
        novoItemLista.id = candidato[1]
        novoItemLista.textContent = candidato[1] + " - " + candidato[2]
        novoItemLista.addEventListener("click",function(){
            selecionar(novoItemLista.id)
        })
        lista.appendChild(novoItemLista)
    }
}